#!/usr/bin/python3

# Copyright (C) 2017 Collabora Ltd
# 2017 Helen Koike <helen.koike@collabora.com>
#
# Ported from bash to python3 by Julien Cristau <jcristau@debian.org>
#
# Copyright (C) 2018 Dropbox, Inc.
# 2018 Luke Faraone <lfaraone@debian.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.

import argparse
import hashlib
import json
import os
import subprocess
import sys
import tempfile
import requests
import logging
import sqlalchemy
import yaml

try:
    from requests_file import FileAdapter
    HAS_FILEADAPTER = True
except ImportError:
    HAS_FILEADAPTER = False

from db import AuditLog, DBSession, PackageState


class Job:
    __slots__ = 'package', 'architecture', 'version', 'tempdir', 'archive', \
                'suite_codename', 'template_unpack_dir', 'template_source_dir', \
                'files_json', 'changes_file_name',

    def __init__(self, package, architecture, suite_codename, version, archive):
        self.package = package
        self.architecture = architecture
        self.tempdir = ""
        self.suite_codename = suite_codename
        self.archive = archive
        self.version = version
        self.template_unpack_dir = None
        self.template_source_dir = None
        self.files_json = None
        self.changes_file_name = ''

    def __str__(self):
        # return a name: value for each field in the object
        return ", ".join(["{}: {}".format(x, str(getattr(self, x))) for x in self.__slots__])

    @property
    def bin_pkgs(self):
        return self.files_json['packages']


config = {
    'commands': {
        # path to the sign-file command from Linux
        'linux_sign_file': '/usr/lib/linux-kbuild-4.9/scripts/sign-file',
        # path to our pesign wrapper script
        'sign-efi': '/usr/local/bin/pesign-wrap'
    },
    'efi': {
        # pkcs11 uri from `p11tool --list-token-urls`
        'pkcs11_uri': None,
        # path to the PEM or DER-format certificate
        'cert_path': None,
        # path to the nss store
        'certdir': '/srv/codesign/pki',
        # name of the token in the nss store
        'token': 'PIV_II (PIV Card Holder pin)',
        # name of the cert in the nss store
        'certname': 'Certificate for Digital Signature',
        'pin': None,
        # file containing PIN
        'pinfile': None,
    },
    'chdist': {
        'dir': os.path.expanduser(os.path.join("~", ".chdist")),
    },
    'archives': {
        # This is also used as the dput target name
        "ftp-master": {
            "deb": [
                {
                    "url": "http://deb.debian.org/debian",
                },
                {
                    "url": "http://incoming.debian.org/debian-buildd",
                    "prefix": "buildd-",
                },
            ],
            "requests": "https://incoming.debian.org/debian-buildd/project/external-signatures/requests.json",  # + .gpg
        },
        "security-master": {
            "deb": [
                {
                    "url": "http://security.debian.org/debian-security",
                },
                {
                    "url": "http://security-master.debian.org/debian-security-buildd",
                    "prefix": "buildd-",
                },
            ],
            "requests": "https://security-master.debian.org/debian-security-buildd/project/external-signatures/requests.json",
        },
    },
    'dput': {
        'config': None,
    },
    'maintainer': {
        'key_id': None,
    },
    'keys': {
        'requests_keyring': 'etc/external-signature-requests.kbx',
        'archive_keyring': None,
        'gnupg_home': None,
        'trusted_certs': [],
    },
    'interactive': False,
}
logging.basicConfig()
logger = logging.getLogger('signer')
logger.setLevel(logging.DEBUG)


def hash_file(f):
    m = hashlib.sha256()
    for c in iter(lambda: f.read(4096), b''):
        m.update(c)
    return m.hexdigest()


def sign_kmod(module_path: str, signature_path: str) -> str:
    env = os.environ.copy()
    env['KBUILD_SIGN_PIN'] = config['efi']['pin']
    # use check_output instead of check_call as sign-file seems to send random
    # stuff to stderr even when it succeeds
    subprocess.check_output(
        [
            config['commands']['linux_sign_file'], '-d', 'sha256',
            config['efi']['pkcs11_uri'], config['efi']['cert_path'], module_path
        ],
        env=env,
        stderr=subprocess.STDOUT
    )

    os.rename(module_path + '.p7s', signature_path)

    with open(signature_path, 'rb') as f:
        fhash = hash_file(f)

    return fhash


def sign_efi(efi_path: str, signature_path: str) -> str:
    env = os.environ.copy()
    if config['efi']['pin'] is not None:
        env['PESIGN_PIN'] = str(config['efi']['pin'])

    with open(signature_path, 'wb') as out:
        subprocess.check_call(
            [
                config['commands']['sign-efi'], config['efi']['certdir'],
                config['efi']['token'], config['efi']['certname'], efi_path
            ],
            env=env, stdout=out
        )

    with open(signature_path, 'rb') as f:
        return hash_file(f)


def update_or_create_chdist(suite_codename, archive):
    dist = "{}-{}".format(suite_codename, archive)

    # `chdist list` blows up if the data-dir does not exist, so short-circuit and create anyways
    chdist_is_initialised = os.path.isdir(config['chdist']['dir'])

    if not chdist_is_initialised or dist not in subprocess.check_output(["chdist", "-d", config['chdist']['dir'], "list"]).decode('ascii').strip():
        # TODO(koike): support more sections.
        # We're creating a chdist here, but we're going to nuke the sources list later, to support multiple sources
        logger.info("Creating new dist {}".format(dist))
        subprocess.check_call(["chdist", "-d", config['chdist']['dir'], "create", dist, config['archives'][archive]['deb'][0]['url'], suite_codename, "main"])

        with open(os.path.join("-d", config['chdist']['dir'], dist, "etc", "apt", "sources.list"),
                  "w") as sourceslist:
            for target in config['archives'][archive]['deb']:
                sourceslist.write(
                    "deb [arch={arches}] {url} {prefix}{suite_codename} {sections}\n".format(
                        arches="amd64,arm64,armhf,i386",
                        url=target['url'],
                        prefix=target.get('prefix', ''),
                        suite_codename=suite_codename,
                        sections="main",
                    )
                )
        if config['keys'].get('archive_keyring'):
            os.symlink(config['keys']['archive_keyring'], os.path.join(config['chdist']['dir'], dist, 'etc', 'apt', 'trusted.gpg.d', 'archive.asc'))

    subprocess.check_call(["chdist", "-d", config['chdist']['dir'], "apt-get", dist, "update"])


def download_pkg(job, pkg):
    logger.info("Downloading: {}".format(pkg))
    subprocess.check_call([
        "chdist", "-d", config['chdist']['dir'], "-a", job.architecture, "apt-get", "{}-{}".format(job.suite_codename, job.archive),
        "download", "{bin}={ver}".format(
            bin=pkg,
            ver=job.version,
        )
    ], cwd=job.tempdir)


def build_pkg_full_name(job, pkg):
    return "{}_{}_{}".format(pkg, job.version, job.architecture)


def extract_pkg(job, pkg):
    pkg = build_pkg_full_name(job, pkg)
    unpack_dir = "/".join([job.tempdir, pkg])
    os.makedirs(unpack_dir)
    subprocess.check_call([
        "dpkg", "-x", pkg + ".deb", unpack_dir
    ], cwd=job.tempdir)
    return unpack_dir


def download_template(job):
    # TODO(lfaraone): need to create per-archive suites
    update_or_create_chdist(job.suite_codename, job.archive)
    download_pkg(job, job.package)


def extract_template(job):
    job.template_unpack_dir = extract_pkg(job, job.package)
    job.template_source_dir = os.path.join(job.template_unpack_dir, "usr", "share", "code-signing", job.package, "source-template")


def get_files_json(job):
    files_json_path = "{base_dir}/usr/share/code-signing/{pkg_name}/files.json".format(base_dir=job.template_unpack_dir,
                                                                                       pkg_name=job.package)
    with open(files_json_path) as fp:
        job.files_json = json.load(fp)


# TODO: optimizations to not iterate over lists again and again
def download_and_extract_binaries(job):
    for pkg in job.bin_pkgs:
        download_pkg(job, pkg)
        extract_pkg(job, pkg)


def check_template(job):
    # Check if mandatory files exist
    assert(os.path.exists(os.path.join(job.template_source_dir, 'debian', 'source', 'format')))
    assert(os.path.exists(os.path.join(job.template_source_dir, 'debian', 'changelog')))
    assert(os.path.exists(os.path.join(job.template_source_dir, 'debian', 'control')))
    assert(os.path.exists(os.path.join(job.template_source_dir, 'debian', 'copyright')))
    assert(os.path.exists(os.path.join(job.template_source_dir, 'debian', 'rules')))
    # Check content of debian/source/format
    with open(os.path.join(job.template_source_dir, 'debian', 'source', 'format'), 'r') as f:
        assert(f.read() == "3.0 (native)\n")
    # Check signatures folder doesn't exist
    assert(not os.path.exists(os.path.join(job.template_source_dir, 'debian', 'signatures')))
    # Check that files.json doesn't use absolute paths
    for pkg, metadata in job.bin_pkgs.items():
        for file in metadata['files']:
            assert(not os.path.isabs(file["file"]))
    # Check that all trusted certificates are whitelisted
    trusted_certs = config['keys']['trusted_certs']
    if '*' not in trusted_certs:
        for pkg, metadata in job.bin_pkgs.items():
            pkg_trusted_certs = metadata.get('trusted_certs')
            assert(pkg_trusted_certs is not None)
            assert(all(cert in trusted_certs for cert in pkg_trusted_certs))


def log_presign(file_path, fhash, package, version, architecture):
    s = DBSession()
    entry = AuditLog(
        binpkg_name=package,
        binpkg_version=version,
        binpkg_architecture=architecture,
        fhash=fhash,
        file_path=file_path,
    )
    s.add(entry)
    s.commit()
    return entry.id


def log_signature(file_path, shash, package, version, presign_id, architecture):
    s = DBSession()
    entry = AuditLog(
        binpkg_name=package,
        binpkg_version=version,
        binpkg_architecture=architecture,
        shash=shash,
        file_path=file_path,
        presign_id=presign_id,
    )
    s.add(entry)
    s.commit()


def sign_and_log_files(job):
    for pkg, metadata in job.bin_pkgs.items():
        for file in metadata['files']:
            relative_file_path = os.path.join(build_pkg_full_name(job, pkg), file["file"])
            file_path = os.path.join(job.tempdir, build_pkg_full_name(job, pkg), file["file"])
            sig_path = os.path.join(job.template_source_dir, 'debian', 'signatures', pkg, file["file"] + ".sig")

            with open(file_path, 'rb') as f:
                fhash = hash_file(f)

            presign_id = log_presign(relative_file_path, fhash, pkg, job.version, job.architecture)

            if not os.path.exists(os.path.dirname(sig_path)):
                os.makedirs(os.path.dirname(sig_path))
            if file["sig_type"] == "efi":
                sig_hash = sign_efi(file_path, sig_path)
            elif file["sig_type"] == "linux-module":
                sig_hash = sign_kmod(file_path, sig_path)
            else:
                raise ValueError("File Type Unknown")
            log_signature(relative_file_path, sig_hash, pkg, job.version, presign_id, job.architecture)


def prepare_source(job):
    subprocess.check_call([
        "dpkg-source", "-b", "."
    ], cwd=job.template_source_dir)
    # ../linux-base_4.5_source.changes
    source_package_name = subprocess.check_output(
        ["dpkg-parsechangelog", "-S", "Source"],
        cwd=job.template_source_dir).decode('ascii').strip()
    source_package_version = subprocess.check_output(
        ["dpkg-parsechangelog", "-S", "Version"],
        cwd=job.template_source_dir).decode('ascii').strip()
    job.changes_file_name = "{}_{}_source.changes".format(source_package_name, source_package_version)
    subprocess.check_call([
        "dpkg-genchanges", "-S", "-DDistribution={}".format(job.suite_codename), "-UCloses", "-O" +
        os.path.join("..", job.changes_file_name),
    ], cwd=job.template_source_dir)


def sign_source(job):
    logger.debug("About to sign {} to {}".format(job.archive, job.changes_file_name))
    env = os.environ.copy()
    gnupg_home = config['keys'].get('gnupg_home')
    if gnupg_home is not None:
        env['GNUPGHOME'] = gnupg_home
    subprocess.check_call(
        ['debsign', '-S', '-k', config['maintainer']['key_id']],
        cwd=job.template_source_dir, env=env)


def submit_source(job):
    env = os.environ.copy()
    gnupg_home = config['keys'].get('gnupg_home')
    if gnupg_home is not None:
        env['GNUPGHOME'] = gnupg_home
    cmd = ['dput']
    if config['dput'].get('config') is not None:
        cmd.extend(['--config', config['dput']['config']])
    cmd.extend(['--', job.archive, os.path.join("..", job.changes_file_name)])
    subprocess.check_call(cmd, cwd=job.template_source_dir, env=env)


def validate_config():
    # XXX: Validate configuration
    # Check that one does not specify both efi.pin and efi.pinfile
    # Check that pesign-wrapper and linux-kbuild exists
    # Check that config object has the right shape
    return


def read_packages_list(url):
    s = requests.Session()
    if HAS_FILEADAPTER:
        s.mount('file://', FileAdapter())

    requests_json = s.get(url)
    requests_gpg = s.get(url + '.gpg')

    # TODO: See if there is a better way to write this
    with tempfile.TemporaryDirectory() as tempdir:
        requests_json_f = os.path.join(tempdir, 'requests.json')
        requests_gpg_f = requests_json_f + '.gpg'
        with open(requests_json_f, "wb") as f:
            f.write(requests_json.content)
        with open(requests_gpg_f, "wb") as f:
            f.write(requests_gpg.content)
        subprocess.check_call(['gpgv', '--keyring', config['keys']['requests_keyring'], requests_gpg_f, requests_json_f])

    return requests_json.json()


def filter_packages(pkgs):
    if len(pkgs) == 0:
        return []

    s = DBSession()
    skippers = s.query(
        PackageState.template_package_name, PackageState.template_package_version,
        PackageState.architecture, PackageState.suite_codename
    ).filter(
        PackageState.template_package_name.in_([pkg['package'] for pkg in pkgs])
    ).having(
        sqlalchemy.or_(
            PackageState.state == 'submitted',
            sqlalchemy.and_(
                PackageState.state == 'failed',
                sqlalchemy.func.count(PackageState.state) >= 3
            )
        )
    ).group_by(
        PackageState.template_package_name, PackageState.template_package_version,
        PackageState.architecture, PackageState.suite_codename,
        PackageState.state
    ).all()

    return [pkg for pkg in pkgs if (
        pkg['package'], pkg['version'], pkg['architecture'], pkg['codename']
    ) not in skippers]


def set_package_state(job: Job, state: str, error_msg=""):
    # TODO(lfaraone): state should be an enum
    s = DBSession()
    entry = PackageState(
        error_msg=error_msg,
        state=state,
        template_package_name=job.package,
        template_package_version=job.version,
        suite_codename=job.suite_codename,
        architecture=job.architecture
    )
    s.add(entry)
    s.commit()


def get_pending(archive, url):
    packages = read_packages_list(url)['packages']
    packages = filter_packages(packages)
    for pkg in packages:
        logger.debug("pkg %s", pkg)
        yield Job(package=pkg["package"],
                  architecture=pkg["architecture"],
                  suite_codename=pkg["codename"],
                  version=pkg["version"],
                  archive=archive)


def process(job):
    with tempfile.TemporaryDirectory(prefix="codesign") as tempdir:
        try:
            job.tempdir = tempdir
            set_package_state(job, 'incomplete')
            download_template(job)
            extract_template(job)
            get_files_json(job)
            check_template(job)
            download_and_extract_binaries(job)
            sign_and_log_files(job)
            prepare_source(job)
            sign_source(job)
            set_package_state(job, 'signed')
            submit_source(job)
            set_package_state(job, 'submitted')
        except Exception as e:
            maybe_interactive_wait("Something went wrong, you can "
                                   "inspect the build to figure out what. "
                                   "The temporary directory is {}\n{}".format(tempdir, e))
            raise


def maybe_interactive_wait(prompt=""):
    if not config['interactive']:
        return
    if prompt:
        print(prompt)
        print("")
    print("Press enter to continue")
    sys.stdin.readline()


def main():
    parser = argparse.ArgumentParser(
        description='sign files for secure boot'
    )
    parser.add_argument(
        '--config', '-c', type=str, default='/etc/codesign.yaml',
        help='configuration file')
    args = parser.parse_args()
    with open(args.config) as fp:
        cp = yaml.safe_load(fp)
    config.update(cp)
    logger.debug("Configuration: {}".format(cp))
    validate_config()
    if config['efi'].get('pinfile', None) is not None:
        with open(config['efi']['pinfile'], 'r') as f:
            config['efi']['pin'] = f.read().strip()
    maybe_interactive_wait("About to start processing archives")
    success = True
    for archive, metadata in config['archives'].items():
        pending = get_pending(archive, metadata['requests'])
        for p in pending:
            try:
                logger.debug("Next job: {}".format(p))
                maybe_interactive_wait("About to start next job")
                process(p)
            except Exception as e:
                success = False
                set_package_state(p, 'failed', str(e))
                logging.exception("")
    return 0 if success else 1


if __name__ == '__main__':
    sys.exit(main())
